import { Component, OnInit } from '@angular/core';
import jsPDF from 'jspdf';
import { ExporterService } from 'src/app/services/exporter.service';


export interface PeriodicElement {
  nombre: string;
  id: number;
  edad: number;
  genero: string;
}



const ELEMENT_DATA: PeriodicElement[] = [
  {id: 1, nombre: 'Juan', edad: 23, genero: 'Masculino'},
  {id: 2, nombre: 'Matias', edad: 44, genero: 'Masculino'},
  {id: 3, nombre: 'Maria', edad: 21, genero: 'Femenino'},
  {id: 4, nombre: 'Luisa', edad: 24, genero: 'Femenino'},
  {id: 5, nombre: 'Rolando', edad: 41, genero: 'Masculino'},
  {id: 6, nombre: 'Messi', edad: 35, genero: 'Masculino'},
  {id: 7, nombre: 'Alex', edad: 33, genero: 'Masculino'},
  {id: 8, nombre: 'Mateo', edad: 11, genero: 'Masculino'},
  {id: 9, nombre: 'Patricia', edad: 65, genero: 'Femenino'},
  {id: 10, nombre: 'Noelia', edad: 22, genero: 'Femenino'},
];


@Component({
  selector: 'app-endpoint',
  templateUrl: './endpoint.component.html',
  styleUrls: ['./endpoint.component.css']
})
export class EndpointComponent implements OnInit {
  displayedColumns: string[] = ['id', 'nombre', 'edad', 'genero'];
  dataSource = ELEMENT_DATA;
  clickedRows = new Set<PeriodicElement>();

  constructor(private excelService: ExporterService) { }

  ngOnInit(): void {
  }

  exportAsXLSX(): void{
    this.excelService.exportToExcel(this.dataSource, 'my export')
  }

  exportAsPDF(): void{
    const doc = new jsPDF();

    doc.html(document.getElementById('form'),10,10);
    doc.save('Tabla de endpoints');
  }


}
